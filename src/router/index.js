import Home from '../views/Home.vue'
const Gallery = () => import('../views/Gallery.vue')
import Login from '../views/Login.vue'
import Register from '../views/Register.vue'
import Profile from '../views/Profile.vue'

const routes = [
   {
      path:'/',
      component: Home
   },
   {
      path:'/gallery',
      component: Gallery
   },
   {
      path:'/login',
      component: Login,
      name: 'Login'
   },
   {
      path:'/register',
      component: Register
   },
   {
      // need auth
      path:'/profile/:userId',
      component: Profile,
      name: 'Profile'
   }
]

export default routes