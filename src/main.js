import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import routes from './router/index'
import './index.css'
import App from './App.vue'

const router = createRouter({
   history: createWebHistory(),
   routes
})

router.beforeEach((to, from, next) => {
   let getToken = localStorage.getItem('token')
   if(to.name === 'Profile' & !getToken) next({name: 'Login'}) 
   else next()
 })

const app = createApp(App)
app.use(router)
app.mount('#app')
